﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataManager
{
    class Manager
    {
      
        /// <summary>
        /// This method adds a new entry
        /// </summary>
        public static void Add()
        {
            Console.Write("Input entry:\t");
            string entry = Console.ReadLine();

            using (FileStream stream = new FileStream(@"C:\fileManagerFile.txt", FileMode.Append, FileAccess.Write, FileShare.Read))
            {
                using (StreamWriter write = new StreamWriter(stream)) 
                {
                    write.WriteLine(entry);
                }
            }
        }

        /// <summary>
        /// This method searches for the occurence of the searche string in the file
        /// </summary>
        /// <returns>This method returns either true or false depending on if the search was successful</returns>
        public static bool Search()
        {
            bool found = false;

            Console.Write("Enter Search Value:\t");
            string searchVal = Console.ReadLine();

            using (StreamReader reader = new StreamReader(@"C:\fileManagerFile.txt"))
            {

               string TextOfFile = reader.ReadToEnd();

               if (TextOfFile.Contains(searchVal))
               {
                   Console.WriteLine("String found at {0}", TextOfFile.IndexOf(searchVal, 0));
               }
               else
               {
                   Console.WriteLine("No match found!");
               }
            }

            return found;

        }

        /// <summary>
        /// This function displays data from the file
        /// </summary>
        public static void ViewData()
        {
            using (FileStream stream = new FileStream(@"C:\fileManagerFile.txt",FileMode.OpenOrCreate,FileAccess.Read,FileShare.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    Console.WriteLine("Data in the File\n");
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }


    }
}
