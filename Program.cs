﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDataManager
{
    class Program
    {
       
        static void Main(string[] args)
        {

            int options;

            Console.WriteLine("File Data Project\n");

            //startup option
           
            //If code reaches here option is valid
            string Continue = "y"; ;
            do
            {
                Console.WriteLine("Please enter an option Fromm Below:\n");
                Console.WriteLine("1-add\t2-search\t3-View Data");

                bool valid = int.TryParse(Console.ReadLine(), out options);

                //If invalid entry of option
                while (!valid)
                {
                    Console.WriteLine("Invalid Data");
                    Console.WriteLine("Please enter an option Fromm Below:\n");
                    Console.WriteLine("1-add\t2-search\t3-View Data");
                    valid = int.TryParse(Console.ReadLine(), out options);

                }

                switch (options)
                {
                    case 1:
                        Manager.Add();
                        break;
                    case 2:
                        Manager.Search();
                        break;
                    case 3:
                        Manager.ViewData();
                        break;
                }

                Console.Write("Enter N to quit:\t");
                Continue = Console.ReadLine().ToUpper();

            } while (Continue != "N");


        }
    }
}
